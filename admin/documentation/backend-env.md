# Backend Environment Variables

## Variables

### `FLASK_APP_BASE_URL`

- **Description**: The base URL of the Flask application.
- **Usage**: Used for configuring the root URL for the application, especially when deploying on different environments.

### `SECRET_KEY`

- **Description**: A secret key for the Flask application.
- **Usage**: Essential for maintaining session security in Flask. 

### `DEPLOY_ON_CLUSTER`

- **Description**: Indicator of whether the app is deployed on a university cluster or elsewhere.
- **Usage**: Used to determine environment-specific configurations, such as CORS settings.

### `MYSQL_SERVICE_HOST`

- **Description**: Hostname of the MySQL service.
- **Usage**: Required for connecting to the MySQL database in production environments.

### `MYSQL_SERVICE_PORT`

- **Description**: Port number for the MySQL service.
- **Usage**: Used alongside the host to establish a connection to the MySQL database.

### `MYSQL_ROOT_PASSWORD`

- **Description**: The root password for MySQL.
- **Usage**: Used for authenticating the root user of the MySQL database

### `MYSQL_PASSWORD`

- **Description**: The password for non-root MySQL users.
- **Usage**: Authentication for MySQL users other than root.

### `mysql_username`

- **Description**: Username for MySQL database access.
- **Usage**: Used for MySQL database connections.

### `mysql_name`

- **Description**: The name of the MySQL database.
- **Usage**: Specifies which MySQL database to connect to.

### `STUDENT_LOOKUP_API_URL`

- **Description**: Base URL for the student directory API.
- **Usage**: Used for fetching student information from an external VT API.

### `STUDENT_LOOKUP_API_TOKEN`

- **Description**: Authentication token for the student directory API.
- **Usage**: Required for authorized access to the student directory API.
