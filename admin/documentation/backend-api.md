# Backend API

## Authentication

### Login
- **URL**: `/api/cas/login`
- **Method**: `GET`
- **Description**: Handles user login via CAS.

### Logout
- **URL**: `/api/cas/logout`
- **Method**: `GET`
- **Description**: Logs out the user from the system.

### User Profile
- **URL**: `/api/cas/profile`
- **Method**: `GET`
- **Description**: Retrieves the profile information of the logged-in user.

## Reports

### Get Reports
- **URL**: `/api/events/get_active_event`
- **Method**: `GET`
- **Description**: Fetches all reports for the current career fair.

### Delete Report
- **URL**: `/api/events/delete/<event_name>`
- **Method**: `POST`
- **Description**: Deletes a specified report.

### Set Active Report
- **URL**: `/api/events/set_active/<selected_event>`
- **Method**: `POST`
- **Description**: Sets a selected event as active for reporting.

### Upload CSV
- **URL**: `/api/upload/<table_name>`
- **Method**: `POST`
- **Description**: Uploads CSV file content to a specified table in the database.

## Student Information

### Get Check-ins
- **URL**: `/api/events/get_students_info/<event_name>`
- **Method**: `GET`
- **Description**: Retrieves check-in information for a specific event.

### Get Student
- **URL**: `/api/student/<int:student_number>`
- **Method**: `GET`
- **Description**: Fetches information about a student based on their student number.

### Append Student
- **URL**: `/api/student/append_student`
- **Method**: `POST`
- **Description**: Adds or updates a student's data in the check-in table.

### VT Endpoint: Lookup Student Info
- **URL**: `/api/student/lookup_student_info/<passport>`
- **Method**: `GET`
- **Description**: Retrieves student information from the VT API using a passport ID.

