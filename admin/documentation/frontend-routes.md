# Frontend Routes 

## Company/Admin Login: `/login`

- **Path**: `/login`
- **Component**: `CASLoginPage`
- **Description**: 
  - This route serves as the entry point for company representatives and admin users to log in.
  - It uses CAS (Central Authentication Service) for authentication.
  - Upon successful login, administrators can view and create different events and track Career Fair data

## Main Dashboard: `/main`

- **Path**: `/main`
- **Component**: `Dashboard`
- **Description**: 
  - This is the main dashboard of the application.
  - It serves as the central hub for administrators and allows them to create an event and manage
  existing events and data

## HTML Output: `/output`

- **Path**: `/output`
- **Component**: `HtmlOutput`
- **Description**: 
  - This route displays HTML output, which may include data or results from various interactions within the application.
  - The html output page displays data of a student after it's manually queried or done via magtek reader

## Card Swipe Main: `/card-swipe`

- **Path**: `/card-swipe`
- **Component**: `CardSwipeMain`
- **Description**: 
  - This route is dedicated to the Card Swipe functionality.
  - It interfaces with the Magtek card reader and is used for student check-ins or information retrieval.
  - The route facilitates the scanning of student IDs and querys the VT api and existing databases for information

## Default Redirect

- **Path**: `*` (Wildcard for all other paths)
- **Action**: Redirects to `/main`
- **Description**: 
  - This is a catch-all route.
  - Users attempting to access any undefined routes are automatically redirected to the main dashboard (`/main`).
