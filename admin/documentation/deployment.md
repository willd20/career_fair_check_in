
# Local Deployment

## Pre-requisites

- Docker installed on the deployment machine.
- Environment variables for the backend application should be set up as per the production environment requirements.
- Access to the project's source code for both backend and frontend applications.

## Backend Deployment

### Building the Docker Image

Navigate to the directory containing the `Dockerfile` for the backend and build the Docker image:

```sh
cd path_to_backend
docker build -t backend-app .
```

### Understanding the Dockerfile

The backend `Dockerfile` performs the following actions:

- Uses `python:3.9` as the base image.
- Exposes port 5000, the default for Flask applications.
- Sets the working directory to `/app`.
- Copies the `requirements.txt` file and installs Python dependencies.
- Copies the source code to the working directory.
- Runs the Flask app using `gunicorn` on port 5000.

### Running the Docker Container

Run the Docker container with the following command, replacing `<env_variables>` with the necessary environment variables:

```sh
docker run -d -p 5000:5000 --env-file <env_variables> backend-app
```

## Frontend Deployment

### Building the Docker Image

Navigate to the directory containing the `Dockerfile` for the frontend and build the Docker image:

```sh
cd path_to_frontend
docker build -t frontend-app .
```

### Understanding the Dockerfile

The frontend `Dockerfile` performs the following actions:

- Uses `node:16` as the base image.
- Sets the working directory to `/app`.
- Copies `package.json` and `package-lock.json` files and installs npm dependencies.
- Copies the source code to the working directory.
- Starts the React application using `npm run start`.

### Running the Docker Container

Run the Docker container with the following command:

```sh
docker run -d -p 3000:3000 frontend-app
```

## Frontend Simplified

Keep in mind this is a very basic explanation if you don't want to utilize the dockerfile as previously stated.

The frontend utilizes NPM so ensure that NPM is installed on the local device

```sh
npm install
npm start
```

After all packages are installed run npm start in the frontend folder and it will run the front end automatically on
local host: 3000.

[Local Host](http://localhost:3000/)

## Service Configuration

Configure the backend service URL in the frontend application through the environment variable `REACT_APP_API_BASE_URL`. This can be set in a `.env` file in the frontend application's root directory.

## Testing the Deployment

Verify that the backend and frontend services are running correctly by accessing:

- Backend: `http://localhost:5000/api/<endpoint>`
- Frontend: `http://localhost:3000`

Ensure that the frontend can communicate with the backend by testing the API endpoints through the frontend application.

## Troubleshooting

Common issues may include port conflicts, environment variable misconfiguration, or Docker daemon issues. Ensure that the Docker service is running and logs are checked for any errors that may give insight into deployment issues.

Common k8 errors can also be resolved by evaluating logs

```sh
kubectl log
```
