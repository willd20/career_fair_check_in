import React from 'react';

const AuthContext = React.createContext({
    isAuthenticated: null as boolean | null,
    setIsAuthenticated: (value: boolean | null) => {}
});

export default AuthContext;