import { Dashboard } from './pages/Dashboard';
import CASLoginPage from './pages/CASLoginPage';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import { Navigate } from 'react-router-dom';
import { useState } from 'react';
import AuthContext from './context/AuthContext';
import HtmlOutput from './pages/HtmlOutput';
import CardSwipeMain from './pages/CardSwipe';

export default function App() {
  const [isAuthenticated, setIsAuthenticated] = useState<boolean | null>(null);

  return (
    <AuthContext.Provider value={{ isAuthenticated, setIsAuthenticated }}>
      <Router>
        <Routes>
          <Route path="/login" element={<CASLoginPage />} />
          <Route path="/main" element={<Dashboard />} />
		  <Route path="/output" element={<HtmlOutput />} />
		  <Route path="/card-swipe" element={<CardSwipeMain />} />
          <Route path="*" element={<Navigate to="/main" />} /> {/* Default redirect */}
        </Routes>
      </Router>
    </AuthContext.Provider>
  );
}
