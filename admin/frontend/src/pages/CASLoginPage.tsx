import { useContext, useEffect, FC } from 'react';
import { useNavigate } from 'react-router-dom';
import AuthContext from '../context/AuthContext';

import Button from '@mui/material/Button';
import Avatar from '@mui/material/Avatar';
import CssBaseline from '@mui/material/CssBaseline';
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import { ThemeProvider } from '@mui/material/styles';
import theme from '../theme';


const API_BASE_URL = process.env.REACT_APP_API_BASE_URL || 'http://localhost:5000'; 

function Copyright(props: any) {
    return (
      <Typography variant="body2" color="text.secondary" align="center" {...props}>
        {'Copyright © CheckPoint '}
        {new Date().getFullYear()}
        {'.'}
      </Typography>
    );
  }

const CASLoginPage: FC = () => {
    const navigate = useNavigate();
    const { isAuthenticated, setIsAuthenticated } = useContext(AuthContext);

    const handleRedirect = () => {
        setIsAuthenticated(true);
        navigate('/main');
    };

    const handleRedirectCardSwipe = () => {
        setIsAuthenticated(true);
        navigate('/card-swipe');
    };

    //console.log(isAuthenticated)
    useEffect(() => {
        // Only check profile if deployed on cloud
        if (process.env.REACT_APP_DEPLOYED_ON_CLOUD === "yes") {
            // On component mount, check for authentication
            fetch(`${API_BASE_URL}/api/cas/profile`)
                .then(response => response.json())
                .then(data => {
                    if (data.username) {
                        handleRedirect();
                    } else {
                        setIsAuthenticated(false);
                    }
                })
                .catch(error => {
                    console.error("Error checking CAS authentication state:", error);
                    setIsAuthenticated(false);
                });
        }
    }, [navigate]);

    const handleLogin = () => {
        // Start CAS login process
        fetch(`${API_BASE_URL}/api/cas/login`)
            .then(response => response.json())
            .then(data => {
                if (data.redirect_url) {
                    window.location.href = data.redirect_url; // Redirect to CAS login page
                } else if (data.message === 'Logged in!') {
                    handleRedirect();
                }
            })
            .catch(error => {
                console.error("Error during CAS login:", error);
            });
    };

    return (
        <ThemeProvider theme={theme}>
            <Grid container component="main" sx={{ height: '100vh' }}>
            <CssBaseline />
            <Grid
                item
                xs={false}
                sm={4}
                md={7}
                sx={{
                backgroundImage: 'url(https://source.unsplash.com/random?wallpapers)',
                backgroundRepeat: 'no-repeat',
                backgroundColor: (t) =>
                    t.palette.mode === 'light' ? t.palette.grey[50] : t.palette.grey[900],
                backgroundSize: 'cover',
                backgroundPosition: 'center',
                }}
            />
            <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
                <Box
                sx={{
                    my: 8,
                    mx: 4,
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                }}
                >
                <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
                    <LockOutlinedIcon />
                </Avatar>
                <Typography sx={{mt : 0.5}} component="h1" variant="h6">
                    Career Fair Check In Admin Page
                </Typography>
                <Button
                type="submit"
                fullWidth
                variant="contained"
                sx={{ mt: 2, mb: 0 }}
                onClick={handleLogin}
                >
                Login with CAS
                </Button>

                {process.env.REACT_APP_DEPLOYED_ON_CLOUD === undefined && (
                    <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    sx={{ mt: 1, mb: 0 }}
                    onClick={handleRedirect}
                    >
                    Bypass Login (Test)
                    </Button>
                )}
                <Button 
                    onClick={handleRedirectCardSwipe} 
                    fullWidth
                    variant="contained"
                    sx={{ mt: 1, mb: 0 }}
                    >
                    Card Swipe Application
                </Button>
                <Copyright sx={{ mt: 5 }} />
                </Box>
				
            </Grid>
            </Grid>
        </ThemeProvider>
    );
}

export default CASLoginPage;