//@ts-nocheck
import { useEffect } from 'react';
import { useNavigate, useLocation } from 'react-router-dom';

const Output = () => {
  const navigate = useNavigate();
  const location = useLocation();
  // Retrieve studentInfo from location state
  const { studentInfo } = location.state || {};

  const getCurrentSemester = () => {
    const month = new Date().getMonth(); // getMonth() returns 0 for January, 1 for February, etc.
  
    if (month >= 0 && month <= 4) { // January to May
      return 'Spring';
    } else if (month >= 5 && month <= 7) { // June to July
      return 'Summer';
    } else { // August to December
      return 'Fall';
    }
  };

  const getMajorTag = (major) => {
    const csMajors = [
      "Computer Science", "Secure Computing", "Data-Centric Computing", 
      "Computer Science & Applications", "Computer Science Minor"
    ];

    if (csMajors.includes(major)) {
      return "**CS**";
    } else {
      return "";
    }
  };
  
  // Calculate current semester and year (this is a placeholder, adjust as needed)
  const currentSemester = getCurrentSemester();
  const currentYear = new Date().getFullYear();
  const tag = getMajorTag(studentInfo.Major);

  useEffect(() => {
    // Print the page when it loads
    window.print();
    // Redirect back to card swipe page after a brief pause
    const timeout = setTimeout(() => {
      navigate('/card-swipe');
    }, 2000);

    return () => clearTimeout(timeout);
  }, [navigate]);

  return (
    <>
      <style>
        {`
          @media print {
            @page {
              size: auto;
              margin: 0mm;
            }
            body * {
              visibility: hidden;
            }
            .print-content, .print-content * {
              visibility: visible;
            }
            .print-content {
              position: absolute;
              left: 0;
              top: 0;
              width: 100%;
            }
          }
        `}
      </style>
      <div className="print-content" style={{ float: 'left', width: '400px' }}>
        <div style={{ float: 'left', marginTop: '25px', marginBottom: '20px', fontSize: '15pt' }}>
          <div>CS|Source {currentSemester} {currentYear}</div>
        </div>

        <div style={{ float: 'right', marginTop: '25px', marginBottom: '20px', fontSize: '15pt', fontFamily: 'Arial Black' }}>
          <div>{tag}</div>
        </div>

        <div style={{ float: 'left', width: '100%', marginBottom: '20px', textAlign: 'center', fontWeight: 'bold', lineHeight: '80%', fontSize: '40pt' }}>
          <div>{studentInfo.Name}</div>
        </div>

        <div style={{ float: 'left', width: '100%', textAlign: 'center', fontSize: '22pt', lineHeight: '90%' }}>
          <div>{studentInfo.Year}</div>
          <br />
          <div>{studentInfo.Major}</div>
        </div>
      </div>
    </>
  );
};

export default Output;
