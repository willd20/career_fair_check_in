import { ThemeProvider } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import List from '@mui/material/List';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import { EventsDrawerItems } from '../components/DashboardList';
import theme from '../theme';
import { useNavigate } from 'react-router-dom';
import AuthContext from '../context/AuthContext';
import { useContext, useEffect, useState } from 'react';
import Button from '@mui/material/Button';
import { CreateEventModal } from '../components/CreateEventModal';
import { DashboardDetail } from '../components/DashboardDetail';
import { Drawer } from '../components/Drawer';
import { AppBar } from '../components/AppBar';

// set base URL to localhost if not deployed on cloud
const API_BASE_URL = process.env.REACT_APP_API_BASE_URL || 'http://localhost:5000';


export interface Filters {
  majors: Array<string>,
  freshmanSeminarStudent: boolean,
  transferStudent: boolean
}

export interface Event {
  Active: number;
  ReportName: string;
}

/*
Dashboard: holds the logic for the dashboard page 
*/
export const Dashboard: React.FC = () => {
  const [open, setOpen] = useState(false);
  const navigate = useNavigate();
  const { isAuthenticated, setIsAuthenticated } = useContext(AuthContext);
  const [username, setUsername] = useState<string | undefined>(undefined);
  const [events, setEvents] = useState<Array<Event>>([]);
  const [selectedEvent, setSelectedEvent] = useState<string | undefined>(undefined);

  // open/close events drawer
  const toggleDrawer = () => {
    setOpen(!open);
  };

  const handleLogout = () => {
    fetch(`${process.env.REACT_APP_API_BASE_URL}/api/cas/logout`)
      .then(response => {
        if (!response.ok) {
          throw new Error("Network response was not ok");
        }
        return response.json();
      })
      .then(data => {
        if (data.redirect_url) {
          window.location.href = data.redirect_url;
        } else {
          setIsAuthenticated(false);
          navigate('/login');
        }
      })
      .catch(error => {
        console.error("Error during logout:", error);
        setIsAuthenticated(false);
        navigate('/login');
      });
  };

  const getEvents = () => {
    fetch(`${API_BASE_URL}/api/events/get_active_event`)
      .then(response => response.json())
      .then(data => {
        setEvents(data.reports);
      })
      .catch(error => {
        console.error("Error fetching reports:", error);
      });
  };
  
  // On component mount, check for authentication
  useEffect(() => {
    // Only check profile if deployed on cloud
    if (process.env.REACT_APP_DEPLOYED_ON_CLOUD === "yes") {
      // Check if the user is really authenticated
      fetch(`${process.env.REACT_APP_API_BASE_URL}/api/cas/profile`)
        .then(response => response.json())
        .then(data => {
          if (data.username) {
            setUsername(data.username);
          } else {
            setIsAuthenticated(false);
            navigate('/login');
          }
      })
      .catch(error => {
          console.error("Error checking authentication in Dashboard:", error);
          // On any error, redirect to login for safety
          setIsAuthenticated(false);
          navigate('/login');
        });
    }

    getEvents();
  }, []);

  return (
    <ThemeProvider theme={theme}>
      <Box sx={{ display: 'flex' }}>
        <CssBaseline />
        <AppBar position="absolute" open={open}>
          <Toolbar
            sx={{
              pr: '24px', // keep right padding when drawer closed
            }}
          >
            <IconButton
              edge="start"
              color="inherit"
              aria-label="open drawer"
              onClick={toggleDrawer}
              sx={{
                marginRight: '36px',
                ...(open && { display: 'none' }),
              }}
            >
              <MenuIcon />
            </IconButton>
            <Typography
              component="h1"
              variant="h6"
              color="inherit"
              noWrap
              sx={{ flexGrow: 1 }}
            >
              CS | Source Admin Dashboard
            </Typography>
            <Typography
              component="span"
              variant="body1"
              color="inherit"
              noWrap
              sx={{ marginLeft: 2, marginRight: 2 }}
            >
              Welcome {process.env.REACT_APP_DEPLOYED_ON_CLOUD === "yes" ? username : "Local User"}
            </Typography>
            <Button color="secondary" variant="contained" onClick={handleLogout}>
              Logout
            </Button>
          </Toolbar>
        </AppBar>
        <Drawer variant="permanent" open={open}>
          <Toolbar
            sx={{
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'flex-end',
              px: [1],
            }}
          >
            <IconButton onClick={toggleDrawer}>
              <ChevronLeftIcon />
            </IconButton>
          </Toolbar>
          <Divider />
          <List component="nav">
            <div>
              <EventsDrawerItems reports={events} setSelectedEvent={setSelectedEvent} getEvents={getEvents}/>
              <Divider sx={{ my: 1 }} />
              <CreateEventModal getEvents={getEvents} setSelectedEvent={setSelectedEvent}/>
            </div>
          </List>
        </Drawer>
        <Box
          component="main"
          sx={{
            backgroundColor: (theme) =>
              theme.palette.mode === 'light'
                ? theme.palette.grey[100]
                : theme.palette.grey[900],
            flexGrow: 1,
            height: '100vh',
            overflow: 'auto',
          }}
        >
          <Toolbar />
          <DashboardDetail selectedEvent={selectedEvent} setSelectedEvent={setSelectedEvent} events={events} getEvents={getEvents}/>
        </Box>
      </Box>
    </ThemeProvider>
  );
}