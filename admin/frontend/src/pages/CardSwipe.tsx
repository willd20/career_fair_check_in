import React, { useState, useEffect } from 'react';
import { styled, createTheme, ThemeProvider } from '@mui/material/styles';
import theme from '../theme';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import { Snackbar, CircularProgress, Grid } from '@mui/material';
import MuiAlert, { AlertProps } from '@mui/material/Alert';
import { useNavigate } from 'react-router-dom';

interface StudentInfo {
  StudentNumber: string;
  Major: string;
  Year: string;
  Name: string;
  CS1944: number;
  Transfer: number;
}

const API_BASE_URL = process.env.REACT_APP_API_BASE_URL || 'http://localhost:5000';

const Alert = React.forwardRef<HTMLDivElement, AlertProps>(function Alert(props, ref) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

export default function CardSwipeMain() {
  const navigate = useNavigate();
  const [snackbarOpen, setSnackbarOpen] = useState(false);
  const [snackbarMessage, setSnackbarMessage] = useState('');
  const [studentID, setStudentID] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [studentInfo, setStudentInfo] = useState<StudentInfo>({
    StudentNumber: '',
    Major: '',
    Year: '',
    Name: '',
    CS1944: 0,
    Transfer: 0,
  });

  const handleCloseSnackbar = () => {
    setSnackbarOpen(false);
  };

  const handleApiCallAndNavigation = async (id: string) => {
    setIsLoading(true);
  
    // Check in the base table
    const baseResponse = await fetch(`${API_BASE_URL}/api/student/${id}`, { method: 'GET' });
    const baseData = await baseResponse.json();
  
    // Call Virginia Tech API regardless of base table result
    const vtResponse = await fetch(`${API_BASE_URL}/api/student/lookup_student_info/${id}`, { method: 'GET' });
  
    // if (!vtResponse.ok) {
    //   // Student not found in VT API
    //   setSnackbarMessage('Invalid student number or student not found.');
    //   setSnackbarOpen(true);
    //   setIsLoading(false);
    //   setStudentID('');
    //   return;
    // }
  
    const vtData = await vtResponse.json();

    let major = '';

    if (baseResponse.ok) {
      major = baseData.Major;
    } else if (vtResponse.ok) {
      major = vtData.major;
      if (major == 'Computer Science & Application') {
        major = 'Computer Science & Applications'
      } else if (major == 'Comp Model & Data Anyl') {
        major = 'Computational Modeling & Data Analytics'
      }
    } else {
      major = vtData.major || 'None';
    }

    console.log(baseData.CS1944)
    console.log(vtData)

    // Update studentInfo with VT API data, and Alumni, CS1944, Transfer status from base table
    setStudentInfo({
      StudentNumber: id,
      Major: major,
      Year: vtData.class_level || 'None',
      Name: vtData.display_name || 'Hokie Bird',
      CS1944: baseData.CS1944 || 0,
      Transfer: baseData.Transfer || 0,
    });
  
    
    setIsLoading(false);
    setStudentID('');
  };

  const handleSendStudentInfo = async () => {
    try {
      const response = await fetch(`${API_BASE_URL}/api/student/append_student`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(studentInfo),
      });
  
      const result = await response.json();
      // if (response.ok) {
        // Handle successful response
        setSnackbarMessage('Student check-in successful.');
        setSnackbarOpen(true);
        setTimeout(() => navigate('/output', { state: { studentInfo } }), 500); // Redirect after a brief pause
      // } else {
      //   // Handle errors
      //   setSnackbarMessage(result.message || 'Error appending student data.');
      //   setSnackbarOpen(true);
      // }
      //}
    } catch (error) {
      // Handle network or other errors
      setSnackbarMessage('Network error, please try again.');
      setSnackbarOpen(true);
    }
  };
  

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const input = event.target.value;
    const regex = /;(\d+)=\d+\?/;
    const matches = input.match(regex);

    if (matches && matches[1]) {
      console.log(matches);
      matches[1].replace(";", "");
      setStudentID(matches[1]);
    } else {
      // Allow manual typing without immediately resetting the input
      if (input.length <= 9) {
        setStudentID(input.replace(/\D/g,''));
      }
    }
  };  

  useEffect(() => {
    const handleSwipeInput = async () => {
      if (studentID.length === 9 && /^\d+$/.test(studentID)) {
        await handleApiCallAndNavigation(studentID);
      }
    };

    if (studentID.length >= 9) {
      handleSwipeInput();
    }
  }, [studentID]);

  useEffect(() => {
    // Ensure this runs only if StudentNumber is set to avoid initial run
    if (studentInfo.StudentNumber) {
      handleSendStudentInfo();
    }
  }, [studentInfo]); // Dependency array: Effect runs when studentInfo changes
  

  


  return (
    <ThemeProvider theme={theme}>
      <Grid container spacing={0} direction="column" alignItems="center" justifyContent="center" style={{ minHeight: '100vh' }}>
        <Grid item xs={12}>
          <TextField
            label="Enter Student ID"
            variant="outlined"
            value={studentID}
            onChange={handleChange}
            fullWidth
            autoFocus
          />
        </Grid>
      </Grid>
      <Snackbar open={snackbarOpen} autoHideDuration={6000} onClose={handleCloseSnackbar}>
        <Alert onClose={handleCloseSnackbar} severity="error" sx={{ width: '100%' }}>
          {snackbarMessage}
        </Alert>
      </Snackbar>
      {isLoading && <CircularProgress />}
    </ThemeProvider>
  );
}
