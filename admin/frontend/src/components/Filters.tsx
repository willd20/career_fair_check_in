// @ts-nocheck
import { Fragment } from 'react';
import { Checkbox, FormControlLabel, Box } from '@mui/material';
import Title from './Title';

export default function Filters({ checked, majorCheck, allMajorsChecked, csDepartmentChecked, ceDepartmentChecked, cmdaDepartmentChecked }) {

  const csMajors = (
    <Box sx={{ display: 'flex', flexDirection: 'column', ml: 3 }}>
      <FormControlLabel
        label="Computer Science"
        control={
          <Checkbox 
            checked={checked[0]} 
            onChange={(e) => majorCheck(e, 0)} 
          />}
      />
      <FormControlLabel
        label="Secure Computing"
        control={
          <Checkbox 
            checked={checked[1]} 
            onChange={(e) => majorCheck(e, 1)} 
          />}
      />
      <FormControlLabel
        label="Data-Centric Computing"
        control={
          <Checkbox 
            checked={checked[2]} 
            onChange={(e) => majorCheck(e, 2)} 
          />}
      />
      <FormControlLabel
        label="Computer Science & Application"
        control={
          <Checkbox 
            checked={checked[3]} 
            onChange={(e) => majorCheck(e, 3)} 
          />}
      />
      <FormControlLabel
        label="Computer Science Minor"
        control={
          <Checkbox 
            checked={checked[4]} 
            onChange={(e) => majorCheck(e, 4)} 
          />}
      />
    </Box>
  );

  const ceMajors = (
    <Box sx={{ display: 'flex', flexDirection: 'column', ml: 3 }}>
      <FormControlLabel
        label="Computer Engineering"
        control={
          <Checkbox 
            checked={checked[5]} 
            onChange={(e) => majorCheck(e, 5)} 
          />}
      />
      <FormControlLabel
        label="Control, Robotics & Autonomy"
        control={
          <Checkbox 
            checked={checked[6]} 
            onChange={(e) => majorCheck(e, 6)} 
          />}
      />
      <FormControlLabel
        label="Networking & Cybersecurity"
        control={
          <Checkbox 
            checked={checked[7]} 
            onChange={(e) => majorCheck(e, 7)} 
          />}
      />
      <FormControlLabel
        label="Machine Learning"
        control={
          <Checkbox 
            checked={checked[8]} 
            onChange={(e) => majorCheck(e, 8)} 
          />}
      />
      <FormControlLabel
        label="Software Systems"
        control={
          <Checkbox 
            checked={checked[9]} 
            onChange={(e) => majorCheck(e, 9)} 
          />}
      />
      <FormControlLabel
        label="Chip-Scale Integration"
        control={
          <Checkbox 
            checked={checked[10]} 
            onChange={(e) => majorCheck(e, 10)} 
          />}
      />
    </Box>
  );

const cmdaMajors = (
  <Box sx={{ display: 'flex', flexDirection: 'column', ml: 3 }}>
    <FormControlLabel
      label="Computational Modeling & Data Analytics"
      control={
        <Checkbox 
          checked={checked[11]} 
          onChange={(e) => majorCheck(e, 11)} 
        />}
    />
  </Box>
);

  const departments = (
    <Box sx={{ display: 'flex', flexDirection: 'column', ml: 3 }}>
      <FormControlLabel
        label="Computer Science Department"
        control={
          <Checkbox 
            checked={checked[0] && checked[1] && checked[2] && checked[3] && checked[4]} 
            onChange={csDepartmentChecked} 
          />}
      />
      {csMajors}
      <FormControlLabel
        label="Computer Engineering Department"
        control={
          <Checkbox 
            checked={checked[5] && checked[6] && checked[7] && checked[8] && checked[9] && checked[10]} 
            onChange={ceDepartmentChecked} 
          />}
      />
      {ceMajors}
      <FormControlLabel
        label="Computational Modeling & Data Analytics Department"
        control={
          <Checkbox 
            checked={checked[11]} 
            onChange={cmdaDepartmentChecked} 
          />}
      />
      {cmdaMajors}
    </Box>
  );

  return (
    <Fragment>
      <Title>Filters</Title>
      <FormControlLabel
        label="All Majors"
        control={
          <Checkbox
            checked={checked[0] && checked[1] && checked[2] && checked[3] && checked[4] && checked[5] && checked[6] && checked[7] && checked[8] && checked[9] && checked[10] && checked[11]}
            onChange={allMajorsChecked}
          />
        }
      />
      {departments}
      <FormControlLabel
        label="CS1944 Student"
        control={
          <Checkbox
            checked={checked[12]}
            onChange={(e) => majorCheck(e, 12)}
          />
        }
      />
      <FormControlLabel
        label="Transfer Student"
        control={
          <Checkbox
            checked={checked[13]}
            onChange={(e) => majorCheck(e, 13)}
          />
        }
      />
    </Fragment>
  );
}