import { Dispatch, SetStateAction, FC, MouseEvent, Fragment } from 'react';
import { ListItemButton, ListItemIcon, ListItemText, ListSubheader } from '@mui/material';
import ErrorIcon from '@mui/icons-material/Error';
import AssignmentIcon from '@mui/icons-material/Assignment';

interface ReportProps {
  reports: any;
  setSelectedEvent: Dispatch<SetStateAction<string | undefined>>
  getEvents: () => void;
}

export const EventsDrawerItems: FC<ReportProps> = (props: ReportProps) => {
  const reports = props.reports;
  const setSelectedEvent = props.setSelectedEvent;

  const handleListItemClicked = (event: MouseEvent<HTMLDivElement>) => {
    const reportName = event.currentTarget.innerText;
    setSelectedEvent(reportName);
  };

  if (reports === undefined || Object.keys(reports).length == 0) {
    return (
      <Fragment>
        <ListSubheader component="div" inset>
          Saved Events
        </ListSubheader>
        <ListItemButton disabled>
          <ListItemIcon>
            <ErrorIcon style={{ color: 'red' }}/>
          </ListItemIcon>
          <ListItemText primary="No reports saved" />
        </ListItemButton>
      </Fragment>  
    );
  } else {
    return (
      <Fragment>
        <ListSubheader component="div" inset>
          Saved Events
        </ListSubheader>
        {Object.keys(reports).map(key => (
          <div key={key}>
            {reports[key].Active === 1 ? (
              <ListItemButton onClick={handleListItemClicked}>
                <ListItemIcon>
                  <AssignmentIcon style={{ color: 'green' }}/>
                </ListItemIcon>
                <ListItemText primary={reports[key].ReportName} sx={{ color: 'green', fontWeight: 'bold' }}/>
              </ListItemButton>
            ) : (
              <ListItemButton onClick={handleListItemClicked}>
                <ListItemIcon>
                  <AssignmentIcon/>
                </ListItemIcon>
                <ListItemText primary={reports[key].ReportName} />
              </ListItemButton>
            )}
          </div>
        ))}
      </Fragment>
    );
  }
};


