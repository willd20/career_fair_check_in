import { Dispatch, SetStateAction, FormEvent, FC, useRef, ChangeEvent, useState } from 'react';
import { ListItemButton, ListItemIcon, ListItemText, Modal, Button, TextField, Typography, Box } from '@mui/material';
import AddIcon from '@mui/icons-material/Add';
import CloudUploadIcon from '@mui/icons-material/CloudUpload';
import { styled } from '@mui/material/styles';

const style = {
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '0px solid #000',
    borderRadius: '8px',
    boxShadow: 'inset 0px 0px 40px rgba(0, 0, 0, 0.1)',
    //boxShadow: 24,
    p: 4,
};

const VisuallyHiddenInput = styled('input')({
    clip: 'rect(0 0 0 0)',
    clipPath: 'inset(50%)',
    height: 1,
    overflow: 'hidden',
    position: 'absolute',
    bottom: 0,
    left: 0,
    whiteSpace: 'nowrap',
    width: 1,
});

const maxFileSize = 5 * 1024 * 1024; // 5MB
const API_BASE_URL = process.env.REACT_APP_API_BASE_URL || 'http://localhost:5000';

interface CreateEventModalProps {
    getEvents: () => void;
    setSelectedEvent: Dispatch<SetStateAction<string | undefined>>;
}

export const CreateEventModal: FC<CreateEventModalProps> = (props: CreateEventModalProps) => {

    const getEvents = props.getEvents;
    const setSelectedEvent = props.setSelectedEvent;

    const [open, setOpen] = useState(false);
    const handleOpen = () => {
        setOpen(true);
        handleFileDelete();
    }
    const handleClose = () => setOpen(false);

    const [file, setFile] = useState<File | null>(null);

    const [eventName, setEventName] = useState<string>('');

    const fileInputRef = useRef<HTMLInputElement>(null);


    const handleSubmit = async (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        if (!eventName.trim()) {
            alert("Please specify event name");
            return;
        }

        handleUpload()
        await new Promise(r => setTimeout(r, 500));
        getEvents();
        setSelectedEvent(eventName);
        handleClose();
    };

    const handleFileChange = (event: ChangeEvent<HTMLInputElement>) => {
        const file = event.target.files ? event.target.files[0] : null;
        if (file) {
            if (file.size > maxFileSize) {
                alert("File is too large. Maximum allowed size is 5MB.");
                return;
            }

            if (file.type === "text/csv") {
                setFile(file);
            } else {
                alert("Please upload a CSV file.");
            }
        }
    };

    const handleFileDelete = () => {
        if (fileInputRef.current) {
            fileInputRef.current.value = "";
        }
        setFile(null);
    };    

    const handleUpload = async () => {
        if (file) {
            const formData = new FormData();
            formData.append('file', file);
      
            // Send the file to the backend
            fetch(`${API_BASE_URL}/api/upload/${eventName}`, {
              method: 'POST',
              body: formData,
            })
              .then(response => response.json())
              .catch((error: Error) => {
                console.error('Upload error:', error);
                console.log(error);
                alert("Upload error: " + error.message)
              });
          } else {
            alert("No file selected or file is not a CSV.");
        }
        return true;
    };

    const handleReportChange = (event: ChangeEvent<HTMLInputElement>) => {
        setEventName(event.target.value);
    }

    return (
        <div>
            <ListItemButton onClick={handleOpen}>
                <ListItemIcon>
                    <AddIcon />
                </ListItemIcon>
                <ListItemText primary="Create Event" />
            </ListItemButton>
            <Modal
            open={open}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
            >
                <form onSubmit={handleSubmit}>
                <Box sx={style}>
                    <Typography variant="h6" component="h2">
                        Create Event
                    </Typography>
                    <Typography variant="body2" sx={{ mt: 1 }}>
                        Upload a CSV file to create an event.
                    </Typography>
                    <div>
                        <TextField id="report-name" label="Event Name" variant="outlined" value={eventName} onChange={handleReportChange} sx={{ mt: 2, width: '100%' }}/>
                    </div>
                    <div>
                        <Button component="label" variant="outlined" startIcon={<CloudUploadIcon />} sx={{ mt: 2, width: '100%'}}>
                            Upload CSV file
                            <VisuallyHiddenInput type="file" onChange={handleFileChange} ref={fileInputRef} />
                        </Button>
                        { file && 
                            <div style={{display: 'flex', marginTop: '1vh'}}>
                                <Typography variant="body2" sx={{ mt: 1, width: '100%'}}>
                                    Current file: {file.name}
                                </Typography>
                                <Button 
                                    variant="outlined" 
                                    onClick={handleFileDelete}
                                    size="small">
                                    Delete
                                </Button>
                            </div>

                        }
                    </div>
                    <div style={{ width: '100%', display: 'flex', marginTop: '2.5vh'}}>
                        <Button sx={{ width: '100%' }} onClick={handleClose} variant="contained">
                            Cancel
                        </Button>
                        <Button sx={{ ml: 1, width: '100%' }} type="submit" variant="contained">
                            Submit
                        </Button>
                    </div>
                    </Box>
                </form>
            </Modal>
        </div>
    );
}