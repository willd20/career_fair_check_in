import { MouseEvent, Fragment } from 'react';
import Link from '@mui/material/Link';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Title from './Title';

// Generate Order Data
function createData(
  id: number,
  name: string,
  time: string,
  hokieID: string,
) {
  return { id, name, time, hokieID };
}

const rows = [
  createData(
    0,
    'William Downey',
    '2022-09-27 18:00:00.000',
    'XXXXXX'
  ),
  createData(
    1,
    'Paul McCartney',
    '2022-09-27 18:00:00.000',
    'XXXXXX'
  ),
  createData(
    2, 
    'Tom Scholz', 
    '2022-09-27 18:00:00.000', 
    'XXXXXX'
  ),
  createData(
    3,
    'Michael Jackson',
    '2022-09-27 18:00:00.000',
    'XXXXXX'
  ),
  createData(
    4,
    'Bruce Springsteen',
    '2022-09-27 18:00:00.000',
    'XXXXXX'
  ),
];

function preventDefault(event: MouseEvent) {
  event.preventDefault();
}

export default function Orders() {
  return (
    <Fragment>
      <Title>Recent Check-ins</Title>
      <Table size="small">
        <TableHead>
          <TableRow>
            <TableCell>Name</TableCell>
            <TableCell>Time</TableCell>
            <TableCell align="right">ID</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow key={row.id}>
              <TableCell>{row.name}</TableCell>
              <TableCell>{row.time}</TableCell>
              <TableCell align="right">{row.hokieID}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
      <Link color="primary" href="#" onClick={preventDefault} sx={{ mt: 3 }}>
        See more orders
      </Link>
    </Fragment>
  );
}