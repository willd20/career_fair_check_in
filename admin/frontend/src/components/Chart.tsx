import { FC, Fragment } from 'react';
import { useTheme } from '@mui/material/styles';
import { BarChart } from '@mui/x-charts';
import Title from './Title';
import { cheerfulFiestaPalette } from '@mui/x-charts/colorPalettes';
import { Student } from '../components/DashboardDetail'; // Import Student interface

let checkMap = new Map<number, string>([
  [0, "Computer Science"],
  [1, "Secure Computing"],
  [2, "Data-Centric Computing"],
  [3, "Computer Science & Applications"],
  [4, "Computer Science Minor"],
  [5, "Computer Engineering"],
  [6, "Controls, Robotics & Autonomy"],
  [7, "Networking & Cybersecurity"],
  [8, "Machine Learning"],
  [9, "Software Systems"],
  [10, "Chip-Scale Integration"],
  [11, "Computational Modeling & Data Analytics"],
]);

const filterData = (checkIns: Array<Student> | undefined, filters: any) => {
  if (checkIns === undefined) {
    return [];
  }
  let filteredMajors = new Array<string>();

  for (let i = 0; i < filters.length; i++) {
    if (filters[i]) {
      filteredMajors.push(checkMap.get(i) as string);
    }
  }

  let filteredData = new Array<Student>();
  for (let i = 0; i < checkIns.length; i++) {
    let matchesMajor = filteredMajors.includes(checkIns[i].Major);
    let matchesSeminar = true;
    if (filters[12]) {
      matchesSeminar = checkIns[i].CS1944 == filters[12];
    } else {
      matchesSeminar = true;
    }
    let matchesTransfer = true;
    if (filters[13]) {
      matchesTransfer = checkIns[i].Transfer == filters[13];
    } else {
      matchesTransfer = true;
    }
    if (matchesMajor && matchesSeminar && matchesTransfer) {
      filteredData.push(checkIns[i]);
    }
  }

  return filteredData;
}

interface ChartProps {
  filters: any;
  checkIns: Array<Student> | undefined;
};

export const Chart: FC<ChartProps> = (props: ChartProps) => {

  const filters = props.filters;
  const checkIns = props.checkIns;

  const theme = useTheme();

  let data = filterData(checkIns, filters);
  let s = [];

  for (let i = 0; i < checkMap.size; i++) {
    let maj = checkMap.get(i) as string;
    let majStudent = data.filter((student) => student.Major == maj);
    let majFreshman = majStudent.filter((student) => student.Year == "Freshman" || student.Year == "Freshman Honors").length;
    let majSophomore = majStudent.filter((student) => student.Year == "Sophomore" || student.Year == "Sophomore Honors").length;
    let majJunior = majStudent.filter((student) => student.Year == "Junior" || student.Year == "Junior Honors").length;
    let majSenior = majStudent.filter((student) => student.Year == "Senior" || student.Year == "Senior Honors").length;
    let majGraduate = majStudent.filter((student) => student.Year == "None").length;
    let majSeries = { data: [majFreshman, majSophomore, majJunior, majSenior, majGraduate], stack: '1', label: maj };
    s.push(majSeries);
  }

  return (
    <Fragment>
      <Title>Number of check-ins that match filters: {filterData(checkIns, filters).length}</Title>
      <BarChart
        xAxis={[{ scaleType: 'band', data: ['Freshman', 'Sophomores', 'Juniors', 'Seniors', 'Graduate/Other'] }]}
        series={s}
        height={900}
        slotProps={{ legend: { hidden: true }}}
        colors={ cheerfulFiestaPalette }
      />
    </Fragment>
  );
}