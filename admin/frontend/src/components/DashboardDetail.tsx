
import { Dispatch, SetStateAction, FC, ChangeEvent, useState, useEffect } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import { Container, Grid, Paper, Typography, Box, Button } from '@mui/material';
import Filters from '../components/Filters';
import { Chart } from '../components/Chart';
import { Event } from '../pages/Dashboard';

const API_BASE_URL = process.env.REACT_APP_API_BASE_URL || 'http://localhost:5000';

function Copyright(props: any) {
    return (
      <Typography variant="body2" color="text.secondary" align="center" {...props}>
        {'Copyright © '}
        <RouterLink to="/" style={{ textDecoration: 'none', color: 'inherit' }}>
          CheckPoint
        </RouterLink>{' '}
        {new Date().getFullYear()}
        {'.'}
      </Typography>
    );
}

interface DashboardDetailProps {
    selectedEvent: string | undefined;
    setSelectedEvent: Dispatch<SetStateAction<string | undefined>>;
    events: Array<Event>;
    getEvents: () => void;
}

export interface Student {
    StudentNumber: string;
    Major: string;
    Year: string;
    Name: string;
    CS1944: number;
    Transfer: number;
}

export const DashboardDetail: FC<DashboardDetailProps> = (props: DashboardDetailProps) => {

    const selectedEvent = props.selectedEvent;
    const setSelectedEvent = props.setSelectedEvent;
    const events = props.events;
    const getEvents = props.getEvents;

    const [checked, setChecked] = useState([true, true, true, true, true, true, true, true, true, true, true, true, false, false]);
    const [students, setStudents] = useState<Array<Student>>();

    const majorCheck = (event: ChangeEvent<HTMLInputElement>, n: number) => {
        let temp = [...checked];
        temp[n] = event.target.checked;
        setChecked(temp);
    };

    const allMajorsChecked = (event: React.ChangeEvent<HTMLInputElement>) => {
        setChecked(Array(12).fill(event.target.checked));
    };

    const csDepartmentChecked = (event: React.ChangeEvent<HTMLInputElement>) => {
        setChecked([event.target.checked, event.target.checked, event.target.checked, event.target.checked, event.target.checked, checked[5], checked[6], checked[7], checked[8], checked[9], checked[10], checked[11]]);
    };

    const ceDepartmentChecked = (event: React.ChangeEvent<HTMLInputElement>) => {
        setChecked([checked[0], checked[1], checked[2], checked[3], checked[4], event.target.checked, event.target.checked, event.target.checked, event.target.checked, event.target.checked, event.target.checked, checked[11]]);
    };

    const cmdaDepartmentChecked = (event: React.ChangeEvent<HTMLInputElement>) => {
        setChecked([checked[0], checked[1], checked[2], checked[3], checked[4], checked[5], checked[6], checked[7], checked[8], checked[9], checked[10], event.target.checked]);
    };

    const handleDeleteEvent = () => {
        fetch(`${API_BASE_URL}/api/events/delete/${selectedEvent}`, {
            method: 'DELETE'
        })
        .then(response => {
            if (!response.ok) {
                console.error("Error deleting event: ", response.statusText);
            } else {
                setSelectedEvent(undefined);
                getEvents();
            }
            return response.json();

        })
    };

    const handleSetActiveEvent = () => {
        fetch(`${API_BASE_URL}/api/events/set_active/${selectedEvent}`, {
            method: 'POST'
        })
        .then(response => {
            if (!response.ok) {
                console.error("Error setting event as active: ", response.statusText);
            } else {
                getEvents();
            }
            return response.json();

        })
    };

    const handleDownloadCSV = () => {
        window.open(`${API_BASE_URL}/api/events/check_in/csv/${selectedEvent}`);
    };

    // when a new event is selected, fetch the students that are checked in to that event
    useEffect(() => {
        if (selectedEvent !== undefined) {
            fetch(`${API_BASE_URL}/api/events/get_students_info/${selectedEvent}`)
            .then(response => response.json())
            .then(data => {
                setStudents(data.students);
                console.log(data.students);
            })
            .catch(error => {
                console.error("Error fetching students:", error);
            });
        }
    }, [selectedEvent]);

    return ( 
        <div>
            {selectedEvent ? (
                    <Container maxWidth={false} sx={{ mt: 2, mb: 2 }}>
                    <Paper sx={{display: 'flex', flexDirection: 'row', mb: 2, mt: 2 }}>
                        <Typography sx={{ fontSize: '1.5rem', margin: 2, fontWeight: 'bold', flexGrow: 1}} color="secondary">
                            {selectedEvent}
                        </Typography>

                            { events && events.find(event => event.Active === 1)?.ReportName === selectedEvent  ? (
                                <Button onClick={handleSetActiveEvent} variant="outlined" sx={{ margin: 1 }} disabled>
                                    This event is active
                                </Button>  
                            ) : (
                                <Button onClick={handleSetActiveEvent} variant="outlined" sx={{ margin: 1 }}>
                                    Set event as active
                                </Button>  
                            )}                
                        <Button onClick={handleDeleteEvent} variant="contained" sx={{ margin: 1 }}>
                            Delete event
                        </Button>
                        <Button onClick={handleDownloadCSV} variant="contained" sx={{ margin: 1 }}>
                            Download Check-in CSV
                        </Button>
                    </Paper>

                    <Grid container spacing={2}>
                        <Grid item xs={8} md={4} lg={3}>
                            <Paper
                            sx={{
                                p: 2,
                                display: 'flex',
                                flexDirection: 'column',
                                height: '100%'
                            }}
                            >
                            <Filters 
                                checked={checked} 
                                majorCheck={majorCheck}
                                allMajorsChecked={allMajorsChecked}
                                csDepartmentChecked={csDepartmentChecked}
                                ceDepartmentChecked={ceDepartmentChecked}
                                cmdaDepartmentChecked={cmdaDepartmentChecked}/>
                            </Paper>
                        </Grid>
                        <Grid item xs={12} md={8} lg={9}>
                            <Paper
                            sx={{
                                p: 5,
                                display: 'flex',
                                flexDirection: 'column',
                                height: '100%'
                            }}
                            >
                            <Chart filters={checked} checkIns={students}/>
                            </Paper>
                        </Grid>
                    </Grid>
                    <Copyright sx={{ pt: 4 }} />
                </Container>   
            ) : (
                <Container sx={{ mt: 4, mb: 4, height: '100%', justifyContent: 'center' }}>
                    <Typography sx={{ fontSize: '3rem', fontWeight: 'bold', textAlign: 'center'}}>
                        Please select an event using the menu on the right
                    </Typography>
                </Container> 
            )}
        </div>
    );
}