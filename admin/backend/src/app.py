from flask import Flask, redirect, request, jsonify, Response, session
from flask_cors import CORS
from database import Database
from cas import CASClient
import csv
import io
import os
import MySQLdb
import requests
import pandas as pd

# Use environment variables from university cloud service.
FLASK_APP_BASE_URL = os.environ.get("FLASK_APP_BASE_URL", "*")
SECRET_KEY = os.environ.get("SECRET_KEY", "SOME_SECRET_KEY")

app = Flask(__name__)
app.secret_key = SECRET_KEY
d = Database()
db = d.create_database_engine()
db_name = d.get_database_name()

# Creating the CAS CLIENT
cas_client = CASClient(
    version=2,
    service_url=f"{FLASK_APP_BASE_URL}/api/login?",
    server_url="https://login.cs.vt.edu/cas/",
    # CHANGE: If you want VT CAS, to be used instead of VT CS CAS
    # change the server_url to https://login.vt.edu/profile/cas/
)

# Only allowing API reqeust from FLASK_APP_BASE_URL if the service is deployed on university cloud service.
if os.environ.get("DEPLOY_ON_CLUSTER") == "yes":
    # When deployed on the cluster, allow requests from the internal React service  
    CORS(app, resources={r"/api/*": {"origins": FLASK_APP_BASE_URL}})
else:
    # When running locally, you might want to allow all origins, or specify a different one
    CORS(app, resources={r"/api/*": {"origins": "*"}})

############################## CAS Authentication ENDPOINTS ##############################

@app.route("/api/cas/login", methods=["GET"])
def login():
    if "username" in session:
        return jsonify(message="Logged in!")

    cas_ticket = request.args.get("ticket")
    if not cas_ticket:
        cas_login_url = cas_client.get_login_url()
        return jsonify(redirect_url=cas_login_url)

    (user, _, _) = cas_client.verify_ticket(cas_ticket)
    if not user:
        return jsonify(message="Failed to verify ticket!"), 403

    session["username"] = user
    return redirect(FLASK_APP_BASE_URL)

@app.route("/api/cas/logout", methods=["GET"])
def logout():
    cas_logout_url = cas_client.get_logout_url(FLASK_APP_BASE_URL)
    session.pop("username", None)
    return jsonify(redirect_url=cas_logout_url)

@app.route("/api/cas/profile", methods=["GET"])
def profile():
    if "username" in session:
        return jsonify(username=session.get("username"))
    return jsonify(message="Logged out!"), 403

############################## Database Manipulation ENDPOINTS ##############################

# Get active event on database
@app.route('/api/events/get_active_event', methods=['GET'])
def get_active_event():
    cursor = db.cursor()

    sql = f'''SELECT ReportName, Active from {db_name}.reports;'''
    print(sql)
    try:
        cursor.execute(sql)
        reports = cursor.fetchall()
        reports_data = [{"ReportName": report[0],
                            "Active": report[1]} for report in reports]
        return jsonify(reports=reports_data)
    except Exception as e:
        print(f"Error fetching reports: {e}")
        return jsonify(message="Error fetching reports"), 500
    finally:
        cursor.close()

# Set selected event as active event. 
@app.route("/api/events/set_active/<selected_event>", methods=["POST"])
def set_active_report(selected_event):
    cursor = db.cursor()

    set_all_inactive = f'UPDATE {db_name}.reports SET Active = 0;'
    set_active = f'''UPDATE {db_name}.reports
    SET Active = 1 
    WHERE ReportName = \'{selected_event}\';'''
    print(set_all_inactive)
    print(set_active)

    try:
        db.query(set_all_inactive)
        db.commit()
        db.query(set_active)
        db.commit()
    except Exception as e:
        print(f"Error setting active report: {e}")
        return jsonify(message="Error setting active report"), 500
    finally:
        cursor.close()
    return jsonify(message="Active report set successfully"), 200

# Delete selected event tables (base table, check-in table) and remove the event from reports table
@app.route('/api/events/delete/<event_name>', methods=['DELETE'])
def delete_report(event_name):
    delete_from_reports = f'''DELETE FROM {db_name}.reports WHERE ReportName = \'{event_name}\';'''
    drop_event_table = f'''DROP TABLE IF EXISTS `{event_name}`;'''
    drop_checkin_table = f'''DROP TABLE IF EXISTS `{event_name}_checkin`;'''
    print(delete_from_reports)
    print(drop_event_table)
    print(drop_checkin_table)

    try:
        db.query(delete_from_reports)
        db.query(drop_event_table)
        db.query(drop_checkin_table)
        db.commit()
        return jsonify(message=f"Report {event_name} deleted successfully"), 200
    except Exception as e:
        print(f"Error deleting report: {e}")
        return jsonify(message="Error deleting report"), 500

# Get student information from check-in table
@app.route('/api/events/get_students_info/<event_name>', methods=['GET'])
def get_checkins(event_name):
    cursor = db.cursor()

    # Step 2: Query the selected table for all students
    try:
        sql = f"SELECT * FROM {db_name}.`{event_name}_checkin`"
        print(sql)
        cursor.execute(sql)
        students = cursor.fetchall()
        students_data = [{"StudentNumber": student[0], 
                          "Major": student[1], 
                          "Year": student[2], 
                          "Name": student[3],
                          "CS1944": student[4],
                          "Transfer": student[5]} for student in students]
        return jsonify(students=students_data)
    except Exception as e:
        print(f"Error fetching check-ins: {e}")
        return jsonify(message="Error fetching check-ins"), 500
    finally:
        cursor.close()

# Download db_checkin table as csv
@app.route('/api/events/check_in/csv/<event_name>', methods=['GET'])
def download_csv(event_name):
    cursor = db.cursor()
    try:
        sql = f"SELECT * FROM {db_name}.`{event_name}_checkin`"
        cursor.execute(sql)
        result = cursor.fetchall()

        output = io.StringIO()
        writer = csv.writer(output)

        # Write CSV headers
        writer.writerow(['StudentNumber', 'Major', 'Year', 'Name', 'CS1944', 'Transfer'])

        for row in result:
            writer.writerow(row)

        output.seek(0)
        # Use f-string for dynamic filename
        content_disposition = f"attachment;filename={event_name}_checkin.csv"
        return Response(output, mimetype="text/csv", headers={"Content-Disposition": content_disposition})
    except Exception as e:
        print(f"Error downloading CSV: {e}")
        return jsonify(message="Error downloading CSV"), 500
    finally:
        cursor.close()

# Upload contents of csv file to database
@app.route('/api/upload/<table_name>', methods=['POST'])
def upload_csv(table_name):
    if not table_name or not table_name.strip():
        return jsonify(message="Event name is required"), 400
    table_name = table_name.lower()
    max_file_size = 5 * 1024 * 1024  # 5MB
    if 'file' not in request.files:
        return jsonify(message='No file part'), 400
    file = request.files['file']

    if file.filename == '':
        return jsonify(message='No selected file'), 400
    # Check the file size
    if file.content_length > max_file_size:
        return jsonify(message='File is too large. Maximum allowed size is 5MB.'), 413
    # Check csv validation
    if file and file.filename.endswith('.csv'):
        dataframe = pd.read_csv(file)
        expected_columns = ['StudentNumber', 'Major', 'CS1944', 'Transfer']
        if list(dataframe.columns) != expected_columns:
            return jsonify(message='CSV file not matching with data schema'), 400

        # Create the table if it doesn't exist
        create_table = f'''CREATE TABLE IF NOT EXISTS `{table_name}_checkin` (
            StudentNumber VARCHAR(10) PRIMARY KEY,
            Major VARCHAR(100),
            Year VARCHAR(100),
            Name VARCHAR(100),
            CS1944 INT,
            Transfer INT
        );'''

        print(create_table)
        try:
            db.query(create_table)
            db.commit()
        except Exception as e:
            print(f"Error creating table: {e}")
            return jsonify(message="Error creating table"), 500

        database_connection_string = d.get_database_connection_string()

        # Insert CSV data into the database
        dataframe.to_sql(table_name, database_connection_string, if_exists='append', index=False)
        add_report(table_name)
        return jsonify(message='CSV file uploaded successfully'), 201
    return jsonify(message='Invalid file type'), 400

# Get student information from base table
@app.route('/api/student/<int:student_number>', methods=['GET'])
def get_student(student_number):
    cursor = db.cursor()

    # Step 1: Find the active event's table name
    try:
        cursor.execute("SELECT ReportName FROM reports WHERE Active = 1")
        result = cursor.fetchone()
        if result:
            selected_table = result[0]
        else:
            return jsonify(message="No active event found"), 404
    except Exception as e:
        print(f"Error finding active event: {e}")
        return jsonify(message="Error finding active event"), 500

    # Step 2: Query the selected table for the student
    try:
        sql = f"SELECT * FROM {db_name}.`{selected_table}` WHERE StudentNumber = {student_number}"
        print(sql)
        cursor.execute(sql)
        student = cursor.fetchone()
        if student:
            return jsonify(StudentNumber=student[0], 
                           Major=student[1], 
                           CS1944=student[2], 
                           Transfer=student[3])
        else:
            return jsonify(message=f"Student with student number: {student_number} does not exist"), 404
    except Exception as e:
        print(f"Error fetching student: {e}")
        return jsonify(message="Error fetching student"), 500
    finally:
        cursor.close()

# Append student info from card-swip to active event's check in table.       
@app.route('/api/student/append_student', methods=['POST'])
def append_student():
    data = request.json
    cursor = db.cursor()

    # Step 1: Find the active event's check-in table name
    try:
        cursor.execute("SELECT ReportName FROM reports WHERE Active = 1")
        result = cursor.fetchone()
        if result:
            check_in_table = result[0] + "_checkin"
        else:
            return jsonify(message="No active event found"), 404
    except Exception as e:
        print(f"Error finding active event: {e}")
        return jsonify(message="Error finding active event"), 500

    # Step 2: Insert or Update Student Data in the Check-In Table
    try:
        sql = f'''
        INSERT INTO {db_name}.`{check_in_table}` (StudentNumber, Major, Year, Name, CS1944, Transfer)
        VALUES (%s, %s, %s, %s, %s, %s)
        ON DUPLICATE KEY UPDATE
        Major = VALUES(Major),
        Year = VALUES(Year),
        Name = VALUES(Name),
        CS1944 = VALUES(CS1944),
        Transfer = VALUES(Transfer)
        '''
        print(sql)
        cursor.execute(sql, (data['StudentNumber'], data['Major'], data['Year'], 
                             data['Name'], data['CS1944'], data['Transfer']))
        db.commit()
        return jsonify(message="Student data appended successfully"), 200
    except Exception as e:
        db.rollback()
        print(f"Error appending student data: {e}")
        return jsonify(message="Error appending student data"), 500
    finally:
        cursor.close()

@app.route('/api/student/lookup_student_info/<passport>', methods=['GET'])
def lookup_student_info(passport):
    student_lookup_api_url = os.environ.get("STUDENT_LOOKUP_API_URL")
    student_lookup_api_token = os.environ.get("STUDENT_LOOKUP_API_TOKEN")

    if not student_lookup_api_url or not student_lookup_api_token:
        return jsonify({"error": "Server configuration error"}), 500

    headers = {"Authorization": f"Token {student_lookup_api_token}"}
    response = requests.get(f"{student_lookup_api_url}{passport}", headers=headers)

    if response.status_code != 200:
        return jsonify({"error": "Failed to fetch student info"}), response.status_code

    return jsonify(response.json())

############################## Helper Functions ##############################

def add_report(report_name):
    cursor = db.cursor()

    create_reports_table = f'''CREATE TABLE IF NOT EXISTS {db_name}.reports (
        ReportName VARCHAR(100) PRIMARY KEY,
        Active INT
    );'''
    add_report = f'''INSERT INTO {db_name}.reports (ReportName, Active) VALUES ('{report_name}', 0);'''
    print(create_reports_table)
    print(add_report)

    try:
        db.query(create_reports_table)
        db.query(add_report)
        db.commit()
        return jsonify(message=f"Report {report_name} added successfully"), 200
    except Exception as e:
        print(f"Error adding report: {e}")
        return jsonify(message="Error adding report"), 500
    finally:
        cursor.close()
        
############################## Initializing Flask Server ##############################

if __name__ == "__main__":
    port = int(os.environ.get("PORT", 5000))
    app.run(host="0.0.0.0", port=port)