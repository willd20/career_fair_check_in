import os
import MySQLdb

class Database:
    """Holds connection configuration for connecting to MySQL database"""

    def __init__(self) -> None:
        """Initializes class variables"""
        self.user = "root"
        self.password = "123456"
        self.host = "127.0.0.1"  # this is local host
        self.port = 3306
        self.database_name = "admin"

        self.check_env_variables()
        self.db = MySQLdb.connect(host=self.host, user=self.user, passwd=self.password, db=self.database_name, port=self.port)

        self.database_connection_string = (
            "mysql+mysqldb://"
            + f"{self.user}:{self.password}@"
            + f"{self.host}:{self.port}/{self.database_name}"
        )


    def check_env_variables(self) -> None:
        """Checks for production environment variables."""
        if os.environ.get("DEPLOY_ON_CLUSTER") == "yes":
            print("currently testing in production")
            self.user = os.getenv("mysql_username", self.user)
            if self.user == "root":
                self.password = os.getenv("MYSQL_ROOT_PASSWORD", self.password)
            else:
                self.password = os.getenv("MYSQL_PASSWORD", self.password)
            self.host = os.getenv("MYSQL_SERVICE_HOST", self.host)
            self.port = int(os.getenv("MYSQL_SERVICE_PORT", self.port))
            self.database_name = os.getenv("mysql_name", self.database_name)
        else:
            print("failed to load environment variables.")

    def create_database_engine(self):
        """Creates the database connection with SQLAlchemy"""
        return self.db
    
    def get_database_connection_string(self):
        return self.database_connection_string
    
    def get_database_name(self):
        return self.database_name
