# Virginia Tech Career Fair Support System

- [Table of Contents](#table-of-contents)
  * [Synopsis](#synopsis)
  * [Contributing Members](#contributing-members)
  * [Features](#features)
  * [Basic Design](#basic-design)
    + [Frontend Routes](admin/documentation/frontend-routes.md)
    + [Backend Environment Variables](admin/documentation//backend-env.md)
    + [Backend Server API](admin/documentation/backend-api.md)
  * [Technologies Used](#technologies-used)
  * [Future Enhancements](#future-enhancements)
  * [Acknowledgments](#acknowledgments)
  * [Prerequisites for Deployment](#prerequisites-for-deployment)
  * [Deployment](#deployment)
  * [Contributing](#contributing)
  * [Contact Information](#contact-information)
  * [License](#license)

## Synopsis

This repository hosts the source for the completely integrated CS Source software systems. The original infrastructure consisted of:

* A check-in application for the CS Source career fair
* A sign-in application for interview day
* An employer note-taking / interview scheduling application
* A Card Swipe Application to utilize magtek card reader and query VT API

The goal of this project was to build upon and revamp the original CS Career Fair website, providing website administrators and future developers with new and improved functions to organize and track user data in the coming years while maintaining one cohesive system with a shared database and interface.

## Contributing Members

* Stephen Kim
* William Downey
* Arnay Vohra

## Features

* Easy server setup
* Streamlined check-in and interview scheduling
* CAS Authentication
* Local Testing
* Card Swipe Application
  * Compatible with Magtek card reader and connected to an HTML output page
  * Card data filtered and managed according to FERPA guidelines
  * Queries VT API for student info
* Unified interface for various career fair activities
* Report generation
  * Event creation to organize reports
  * CSV file upload option
  * Generates statistical charts based on career fair attendance and interviews conducted
    * Allows users to set active charts to record data based on event
  * Data Filtering
    * Filtering for Computer Science Department
        * Allows users to filter whether a student is a CS1944 student and transfer student as well

## Basic Design

* Python-based backend
* TypeScript-based frontend
* SQL Database
* CAS authentication for secure logins
* Designed to cater to external usage cases

## Technologies Used

This project utilizes a diverse array of technologies to ensure robust performance and scalable architecture. Below is a breakdown of the key technologies and libraries used:

### Backend

The backend is developed using Flask, a lightweight and powerful web framework for Python. This setup provides a flexible and efficient environment for handling web requests and database interactions. Key technologies in the backend include:

- **Flask 3.0.0**: A web framework for building APIs and handling HTTP requests.
- **Flask-SQLAlchemy 3.1.1**: An ORM extension for Flask, facilitating database interactions.
- **mysqlclient 2.2.0**: A Python library for connecting to MySQL databases.
- **flask-cors 4.0.0**: For handling Cross-Origin Resource Sharing (CORS), essential for frontend-backend communication.
- **python-cas 1.6.0**: Integrates Central Authentication Service (CAS) for secure user authentication.
- **gunicorn 21.2.0**: A WSGI HTTP server for running Python web applications on UNIX systems.
- **pandas 2.1.2**: A data manipulation and analysis library, used for processing complex data operations.
- **requests**: For making HTTP requests to external APIs.

### Frontend

The frontend is crafted using TypeScript and CSS, complemented by Docker for containerization, enhancing deployment and consistency across different environments. The use of Docker encapsulates the frontend environment, ensuring that it runs smoothly regardless of the underlying system. The technologies employed in the frontend include:

- **TypeScript**: Provides static typing to JavaScript, enhancing code quality and maintainability.
- **CSS**: For styling the frontend, ensuring a responsive and aesthetically pleasing user interface.
- **Docker**: Used for containerizing the frontend application, facilitating easier deployment and environment consistency.

The combination of these technologies, from Python and Flask in the backend to TypeScript and Docker in the frontend, forms a cohesive and scalable foundation for the Virginia Tech Career Fair Support System.

## Future Enhancements

* Potential features or improvements planned for future updates.

## Acknowledgments

### Stakeholders
* Professor Charles 
* Benjamin Stein

### Previous Developers
* Samuel Myles
* Uditi Goyal
* JP A.
* Gerrit Crater
* Caroline Lawrence-Hovey

### Prerequisites

Before proceeding with the deployment, ensure the following prerequisites are met:
- Docker is installed on the deployment server.
- You have access to the project's source code.

### Contact Info
* Professor Charles (rich08@vt.edu)
* Benjamin Stein (bbradner@vt.edu)